const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === 'production'
  ? '/' + process.env.CI_PROJECT_NAME + '/'
  : '/',
  transpileDependencies: true,
  pwa: {
    name : "PubHubs PWA iFRAME PoC",
    themeColor: "#408BE1",
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black'
  }
})
